import React, { Component } from 'react';
import './App.css';
import axios from 'axios'
import mobiscroll from '@mobiscroll/react';
import '@mobiscroll/react/dist/css/mobiscroll.min.css';

import PaperCard, {FollowCard} from './Cards'


const config = {
    url: "https://newsapi.org/v2/top-headlines?country=us",
    api_key: "d6f99e4cda3d431aa1124ceaecb3a7d8",

};

const headers = {headers: {'X-Api-Key': config.api_key,
    }};

export default class Home extends Component {

    constructor(props) {
        super(props);
        this.state = {articles: [], papers: []};
    }


    componentDidMount() {
        this.getRecommendedArticles();
        this.getFollowPapers();
    }

    getRecommendedArticles(user_id) {
        axios.get(config.url, headers)
            .then(response => {
                // We expect articles to be a field in data
                // console.log(response.data.articles);
                const articles = response.data.articles;
                this.setState({articles})
            })
            .catch(error => console.log(error));
    }

    getFollowPapers(user_id) {
        axios.get("http://127.0.0.1:5000/friends-recommendation", headers)
            .then(response => {
                // We expect articles to be a field in data
                console.log(response.data.results);
                const papers = response.data.results;
                this.setState({papers})
            })
            .catch(error => console.log(error));
    }

    createPaperCards(papers) {
        const cards = [];
        var count = 1;
        papers.forEach(paper => {
            // For each paper, create a card and pass as a prop to PaperCard Comp.
            cards.push(<PaperCard paper={paper} key={count}/>);
            count += 1
        });
        return cards
    }

    // Create cards for research papers
    createResearchCards(papers) {
        const cards = [];
        var count = 1;
        papers.forEach(paper => {
            // For each paper, create a card and pass as a prop to PaperCard Comp.
            cards.push(<FollowCard paper={paper} key={count}/>);
            count += 1
        });
        return cards
    }

    render() {

        return (

            <div>
                <h3 style={{marginTop: "40px"}}>Recommended</h3>
                <mobiscroll.ScrollView
                    theme="ios"
                    themeVariant="dark"
                    layout="fixed"
                    itemWidth={250}
                    className="md-fixed"
                >
                    {this.createResearchCards(this.state.papers)}
                </mobiscroll.ScrollView>

                <h3>Trending</h3>
                <mobiscroll.ScrollView
                    theme="ios"
                    themeVariant="dark"
                    layout="fixed"
                    itemWidth={250}
                    className="md-fixed"
                >
                    {this.createPaperCards(this.state.articles)}
                </mobiscroll.ScrollView>

                <h3>News Today</h3>

                <mobiscroll.ScrollView
                    theme="ios"
                    themeVariant="dark"
                    layout="fixed"
                    itemWidth={250}
                    className="md-fixed"
                >
                    {this.createPaperCards(this.state.articles)}
                </mobiscroll.ScrollView>


            </div>

        )
    }
}