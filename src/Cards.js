import React, { Component } from 'react';
import mobiscroll from '@mobiscroll/react';


// export default class PaperCard extends Component {
//
//     render() {
//         const {paper} = this.props;
//
//         return(
//                 <Col sm={4}>
//                     <Card>
//                         <Card.Header>{paper.source.name}</Card.Header>
//                         <Card.Body>
//                             <Card.Title>{paper.title}</Card.Title>
//                             <Card.Text>
//                                 {paper.description}
//                             </Card.Text>
//                             <Button variant="primary">Read Now</Button>
//                         </Card.Body>
//                     </Card>
//                 </Col>
//         )
//     }
//  }

export default class PaperCard extends Component {

    openPaper(url, e) {
        e.preventDefault();
        window.open(url, "_blank")
    }

    render() {
        const {paper} = this.props;

        return(
            <mobiscroll.Card theme="ios"  themeVariant="dark">
                <mobiscroll.CardContent>
                    <img className="md-img" src={paper.urlToImage} draggable="false"/>
                    <mobiscroll.CardTitle>{paper.title}</mobiscroll.CardTitle>
                    <mobiscroll.CardSubtitle>{paper.description}</mobiscroll.CardSubtitle>
                </mobiscroll.CardContent>
                <mobiscroll.CardFooter>
                    <button onClick={this.openPaper.bind(this, paper.url)}
                            className="mbsc-btn-flat">Share</button>

                    <button className="mbsc-btn-flat">Read Now</button>
                </mobiscroll.CardFooter>
                </mobiscroll.Card>
        )
    }
}

export class FollowCard extends Component {

    openPaper(url, e) {
        e.preventDefault();
        window.open(url, "_blank")
    }

    render() {
        const {paper} = this.props;

        return(
            <mobiscroll.Card theme="ios"  themeVariant="dark">
                <mobiscroll.CardContent>
                    <img className="md-img" src={paper.urlToImage} draggable="false"/>
                    <mobiscroll.CardTitle>{paper.title.raw}</mobiscroll.CardTitle>
                    <mobiscroll.CardSubtitle>{paper.description}</mobiscroll.CardSubtitle>
                </mobiscroll.CardContent>
                <mobiscroll.CardFooter>
                    <button onClick={this.openPaper.bind(this, paper.url.raw)}
                            className="mbsc-btn-flat">Share</button>

                    <button className="mbsc-btn-flat">Read Now</button>
                </mobiscroll.CardFooter>
            </mobiscroll.Card>
        )
    }
}