import React, { Component, Fragment } from 'react';
import 'react-chat-elements/dist/main.css';
import  {MessageList} from 'react-chat-elements'
import {MessageBox} from "react-chat-elements";
import { ChatItem } from 'react-chat-elements'
import { Input } from 'react-chat-elements'
import mobiscroll, {Button} from "@mobiscroll/react";

const datasource = [{
    position: 'right',
    type: 'text',
    text: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit',
    date: new Date(),
},
    {
        position: 'left',
        type: 'text',
        text: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit',
        date: new Date(),
    },
    {
        position: 'right',
        type: 'text',
        text: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit',
        date: new Date(),
    }

];

const messages = datasource.forEach(message => {
    return (< MessageBox
        position={message.position}
        type={message.type}
        text={message.text} />)
    })

const messageItems = datasource.forEach(message => {
        return (<ChatItem
                    title={"sup"}
                    subtitle={"derp"}
                    date={new Date()}
            />
    )
})


export default class PaperChat extends Component {

    constructor() {
        super();
        this.state = {}
    }

    render() {

        return(
            // TODO Do we use messageItem or MessageBox or is Item in MessageBox
            <div theme="ios" themeVariant="dark">
            <MessageList
                className='message-list'
                lockable={true}
                toBottomHeight={'100%'}
                dataSource={datasource}>
                {messageItems}
            </MessageList>
                <Input
                    placeholder="Type here..."
                    multiline={true}
                    rightButtons={
                        <mobiscroll.Button className="mbsc-btn-flat" theme="ios" themeVariant="dark">Send</mobiscroll.Button>
                    }/>
            </div>


        )
    }

}

