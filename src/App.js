import React, { Component } from 'react';
import { Route, withRouter, BrowserRouter as Router, Switch, Redirect } from 'react-router-dom';
import './App.css';
import mobiscroll from '@mobiscroll/react';
import '@mobiscroll/react/dist/css/mobiscroll.min.css';

import Home from './Home'
import PaperChat from './Components/Chat'
import SearchBar from "./Components/Search";

mobiscroll.setupReactRouter(Route, withRouter);

class App extends Component {

    constructor(props) {
        super(props);
        this.state = {articles: []};
    }

    render() {
        return (
            <Router>
                <div className="md-navigation">
                    <mobiscroll.TabNav
                        theme="ios"
                        themeVariant="dark"
                    >
                        <mobiscroll.NavItem to="/home" icon="empty micons icon-iphone_6">Home</mobiscroll.NavItem>
                        <mobiscroll.NavItem to="/chat" icon="empty micons icon-iphone_6">Chat</mobiscroll.NavItem>
                        <mobiscroll.NavItem to="/search" icon="empty micons icon-iphone_6">Search</mobiscroll.NavItem>

                    </mobiscroll.TabNav>

                    <Switch>
                        <Route path="/home" component={Home}/>
                        <Route path="/chat" component={PaperChat}/>
                        <Route path="/search" component={SearchBar}/>
                        <Redirect path="/" exact to="/home" />
                    </Switch>
                </div>
            </Router>

        );
      }
}

export default App;
